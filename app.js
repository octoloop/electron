const testFolder = './test/';
const fs = require('fs');

var file_list = []
file_list = fs.readdirSync(testFolder);

var re = /(?:\.([^.]+))?$/;

var app = new Vue({
	el: '#main',
	data: {
		current_state: "home_page",
		programs: file_list,
		joints: [
			{label: "Joint 1", value: 0},
			{label: "Joint 2", value: 0},
			{label: "Joint 3", value: 0},
			{label: "Joint 4", value: 0},
			{label: "Joint 5", value: 0},
		],
		selected_pgr: '',
		right_pane_state: 'choice',
		filename: ''
	},
	methods: {
		to_pgr_sys_page: function() {
			this.current_state = 'pgr_sys_page'
		},
		to_home_page: function() {
			this.current_state = "home_page"
		},
		to_load_pgr_page: function() {
			this.current_state = "load_pgr_page"
		},
		delete_file: function(program) {
			fs.unlinkSync(testFolder+program)
			var index = this.programs.indexOf(program)
			this.programs.splice(index,1) 
		},
		refresh_list: function() {
			this.programs = fs.readdirSync(testFolder)
		},
		select_pgr: function(file) {
			this.selected_file = file
		},
		new_file: function() {
			if(this.filename == '')
				alert('Please enter the file name')
			else if(!this.filename.endsWith(".move")){
				this.filename += '.move'
				this.right_pane_state = 'opened_file'
			}
			else
				this.right_pane_state = 'opened_file'
			//todo: save this file in the disc
		},
		edit_file: function(file){
			this.right_pane_state = 'opened_file'
		}
	},
})


